package com.pranitkulkarni.msinusexpensecalculator.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pranitkulkarni.msinusexpensecalculator.model.InformationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pranitkulkarni on 4/12/17.
 */

public class DatabaseManager extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private Context context;

    // Database Name
    private static final String DATABASE_NAME = "p_student_calculator.db";

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
        this.context = context;
    }

    private static final String CREATE_INFORMATION_TABLE =
            "CREATE TABLE " + DatabaseInfo.Information.TABLE_NAME +
            " ( " + DatabaseInfo.Information.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DatabaseInfo.Information.I20_AMOUNT + " INTEGER, "
            + DatabaseInfo.Information.LIVING_EXPENSES + " INTEGER, "
            + DatabaseInfo.Information.STUDENT_FUNDS + " INTEGER, "
            + DatabaseInfo.Information.START_DATE + " TEXT, "
            + DatabaseInfo.Information.END_DATE + " TEXT, "
            + DatabaseInfo.Information.UNIVERSITY_NAME + " TEXT, "
            + DatabaseInfo.Information.COURSE_NAME + " TEXT, "
            + DatabaseInfo.Information.DURATION_IN_MONTHS + " INTEGER, "
            + DatabaseInfo.Information.CALCULATED_FOR + " INTEGER )";

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_INFORMATION_TABLE);

    }


    public Boolean addInfo(int id, String COLUMN_NAME, String VALUE){


        try{

            final SQLiteDatabase database = getWritableDatabase();

            String query = "UPDATE " + DatabaseInfo.Information.TABLE_NAME + " SET " + COLUMN_NAME + " = '"+ VALUE + "' WHERE "
                    + DatabaseInfo.Information.ID +" = "+ id;


            database.execSQL(query);

            database.close();


        }catch (Exception e){
            e.printStackTrace();
            return false;
        }


        return true;
    }

    public Boolean addInfo(int id, String COLUMN_NAME, int VALUE){


        try{

            final SQLiteDatabase database = getWritableDatabase();

            String query = "UPDATE " + DatabaseInfo.Information.TABLE_NAME + " SET " + COLUMN_NAME +" = "+ VALUE + " WHERE "
                    + DatabaseInfo.Information.ID +" = "+ id;


            database.execSQL(query);

            database.close();


        }catch (Exception e){
            e.printStackTrace();
            return false;
        }


        return true;
    }


    public Boolean addDates(int id, String START, String END,int DIFFERENCE){


        try{

            final SQLiteDatabase database = getWritableDatabase();

            String query = "UPDATE " + DatabaseInfo.Information.TABLE_NAME + " SET "
                    + DatabaseInfo.Information.START_DATE +" = '"+ START
                    + "' , " + DatabaseInfo.Information.END_DATE + " = '" + END
                    + "' , " + DatabaseInfo.Information.DURATION_IN_MONTHS + " = " + DIFFERENCE
                    + " WHERE " + DatabaseInfo.Information.ID +" = "+ id;


            database.execSQL(query);

            database.close();


        }catch (Exception e){
            e.printStackTrace();
            return false;
        }


        return true;
    }


    public InformationModel getInfo(int id){

        InformationModel info  = new InformationModel();

        String query = "SELECT * FROM "+ DatabaseInfo.Information.TABLE_NAME + " WHERE " + DatabaseInfo.Information.ID + " = " + id;

        SQLiteDatabase database = getWritableDatabase();
        Cursor cursor = database.rawQuery(query,null);

        if (cursor != null){

            cursor.moveToFirst();

            info.setI20Amount(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.I20_AMOUNT)));
            info.setLiving_expenses(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.LIVING_EXPENSES)));
            info.setStudent_funds(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.STUDENT_FUNDS)));
            info.setDurationInMonths(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.DURATION_IN_MONTHS)));
            info.setCalculated_for(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.CALCULATED_FOR)));

            info.setStartDate(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Information.START_DATE)));
            info.setEndDate(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Information.END_DATE)));
            info.setUniversityName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Information.UNIVERSITY_NAME)));
            info.setCourseName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Information.COURSE_NAME)));


            cursor.close();
        }

        database.close();

        return info;

    }


    public List<InformationModel> getAllInformation(){

        List<InformationModel> list = new ArrayList<>();

        String query = "SELECT * FROM "+ DatabaseInfo.Information.TABLE_NAME;

        SQLiteDatabase database = this.getWritableDatabase();
        Cursor cursor = database.rawQuery(query,null);

        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()){

            InformationModel informationModel = new InformationModel();
            informationModel.setId(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.ID)));
            informationModel.setUniversityName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Information.UNIVERSITY_NAME)));
            informationModel.setCourseName(cursor.getString(cursor.getColumnIndex(DatabaseInfo.Information.COURSE_NAME)));
            informationModel.setI20Amount(cursor.getInt(cursor.getColumnIndex(DatabaseInfo.Information.I20_AMOUNT)));

            if (informationModel.getUniversityName() != null)    // Don't add it to the list if not calculated completely..
                list.add(informationModel);

        }

        cursor.close();
        database.close();

        return list;

    }

    public int createNewEntry(int i20_amount,int months){

        int row_id = 0;

        try {

            SQLiteDatabase database = getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseInfo.Information.I20_AMOUNT,i20_amount);
            contentValues.put(DatabaseInfo.Information.CALCULATED_FOR,months);

            long id = database.insert(DatabaseInfo.Information.TABLE_NAME,null,contentValues);

            Log.d("ID of row created"," -> "+id);

            database.close();


            row_id = (int) id;
        }
        catch (Exception e){
            e.printStackTrace();

        }

        return row_id;

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // TODO
    }
}
