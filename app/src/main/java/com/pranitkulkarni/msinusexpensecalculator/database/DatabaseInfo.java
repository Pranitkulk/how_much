package com.pranitkulkarni.msinusexpensecalculator.database;

import android.provider.BaseColumns;

/**
 * Created by pranitkulkarni on 4/11/17.
 */

public class DatabaseInfo {


    private DatabaseInfo(){}


    public static class Information implements BaseColumns{

        public static final String TABLE_NAME = "information";

        public static final String ID = "id";
        public static final String I20_AMOUNT = "i20_amount";
        public static final String LIVING_EXPENSES = "living_expenses";
        public static final String STUDENT_FUNDS = "student_funds";
        public static final String CALCULATED_FOR = "calculated_for";
        public static final String START_DATE = "program_start_date";
        public static final String END_DATE = "program_end_date";
        public static final String DURATION_IN_MONTHS = "duration_in_months";
        public static final String UNIVERSITY_NAME = "university_name";
        public static final String COURSE_NAME = "course_name";
        
    }


    public static class Calculations implements BaseColumns{

        public static final String TABLE_NAME = "calculations";

        public static final String CALCULATION_ID = "calculation_id";
        public static final String INFO_ID = "info_id"; // Foreign key

        public static final String TOTAL_LIVING_EXPENSE = "total_living_expense";
        public static final String TOTAL_EDUCATION_EXPENSE = "total_education_expense";
        public static final String MONTHLY_LIVING_EXPENSE = "monthly_living_expense";


    }
}
