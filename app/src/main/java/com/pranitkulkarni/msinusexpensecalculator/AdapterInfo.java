package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pranitkulkarni.msinusexpensecalculator.model.InformationModel;

import java.util.List;

/**
 * Created by pranitkulkarni on 5/2/17.
 */

public class AdapterInfo extends RecyclerView.Adapter<AdapterInfo.myViewHolder>{

    private Context context;
    private List<InformationModel> infoList;

    public AdapterInfo(Context context,List<InformationModel> list){

        this.context = context;
        this.infoList = list;

    }

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new myViewHolder(LayoutInflater.from(context).inflate(R.layout.info_list_item,parent,false));
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {

        final InformationModel info = infoList.get(position);
        holder.univName.setText(info.getUniversityName());
        holder.courseName.setText(info.getCourseName());
        holder.i20Amount.setText("$ "+info.getI20Amount());

        holder.clickLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent showExpenses = new Intent(context,ExpensesReceipt.class);
                showExpenses.putExtra("id",info.getId());
                context.startActivity(showExpenses);

            }
        });

    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }


    public static class myViewHolder extends RecyclerView.ViewHolder{

        TextView univName,courseName,i20Amount;
        View clickLayout;

        public myViewHolder(View itemView){

            super(itemView);

            univName = (TextView)itemView.findViewById(R.id.universityName);
            courseName = (TextView)itemView.findViewById(R.id.courseName);
            i20Amount = (TextView)itemView.findViewById(R.id.i20_amount);

            clickLayout = itemView.findViewById(R.id.clickLayout);

        }

    }


}
