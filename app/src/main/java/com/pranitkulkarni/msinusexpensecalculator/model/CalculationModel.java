package com.pranitkulkarni.msinusexpensecalculator.model;

/**
 * Created by pranitkulkarni on 4/21/17.
 */

public class CalculationModel {

    private int id = 0, info_id = 0, total_living_expense = 0, total_education_expense = 0, total_expense = 0, monthly_expense = 0;

    public int getId() {
        return id;
    }

    public int getInfo_id() {
        return info_id;
    }

    public int getMonthly_expense() {
        return monthly_expense;
    }

    public int getTotal_education_expense() {
        return total_education_expense;
    }

    public int getTotal_expense() {
        return total_expense;
    }

    public int getTotal_living_expense() {
        return total_living_expense;
    }

    //

    public void setId(int id) {
        this.id = id;
    }

    public void setInfo_id(int info_id) {
        this.info_id = info_id;
    }

    public void setTotal_education_expense(int total_education_expense) {
        this.total_education_expense = total_education_expense;
    }

    public void setMonthly_expense(int monthly_expense) {
        this.monthly_expense = monthly_expense;
    }

    public void setTotal_expense(int total_expense) {
        this.total_expense = total_expense;
    }

    public void setTotal_living_expense(int total_living_expense) {
        this.total_living_expense = total_living_expense;
    }
}
