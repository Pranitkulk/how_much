package com.pranitkulkarni.msinusexpensecalculator.model;

/**
 * Created by pranitkulkarni on 4/16/17.
 */

public class InformationModel {

    private int id =0 ,i20Amount = 0, living_expenses =  0, student_funds = 0, durationInMonths = 0, calculated_for = 9;
    private String startDate,endDate,universityName,courseName;

    public int getId() {
        return id;
    }

    public int getI20Amount() {
        return i20Amount;
    }

    public int getLiving_expenses() {
        return living_expenses;
    }

    public int getStudent_funds() {
        return student_funds;
    }

    public int getDurationInMonths() {
        return durationInMonths;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getUniversityName() {
        return universityName;
    }

    public int getCalculated_for() {
        return calculated_for;
    }

    // SET METHODS ................


    public void setId(int id) {
        this.id = id;
    }

    public void setI20Amount(int i20Amount) {
        this.i20Amount = i20Amount;
    }


    public void setLiving_expenses(int living_expenses) {
        this.living_expenses = living_expenses;
    }

    public void setDurationInMonths(int durationInMonths) {
        this.durationInMonths = durationInMonths;
    }

    public void setStudent_funds(int student_funds) {
        this.student_funds = student_funds;
    }


    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setUniversityName(String universityName) {
        this.universityName = universityName;
    }

    public void setCalculated_for(int calculated_for) {
        this.calculated_for = calculated_for;
    }
}
