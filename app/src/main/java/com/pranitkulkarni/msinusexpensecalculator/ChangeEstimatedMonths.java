package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChangeEstimatedMonths extends AppCompatActivity {

    SeekBar seekBar;
    int months = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_estimated_months);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView numberOfMonthsText = (TextView)findViewById(R.id.number_of_months);
        seekBar = (SeekBar)findViewById(R.id.seekbar_months);

        months = getIntent().getIntExtra("months",9);
        numberOfMonthsText.setText(String.valueOf(months));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            seekBar.setProgress(months - 1,true);
        else
            seekBar.setProgress(months - 1);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                months = progress+1;
                numberOfMonthsText.setText(String.valueOf(months));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        Intent intent = new Intent();
        intent.putExtra("months",months);
        setResult(RESULT_OK,intent);    // Send back the updated number of months
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            Intent intent = new Intent();
            intent.putExtra("months",months);
            setResult(RESULT_OK,intent);
            finish();
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
