package com.pranitkulkarni.msinusexpensecalculator.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pranitkulkarni.msinusexpensecalculator.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {


    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_settings, container, false);

        rootview.findViewById(R.id.contact_developer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setType("text/plain");
                intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                intent.putExtra(Intent.EXTRA_EMAIL,new String[]{"pranitkulkarni24@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT,"Feedback");
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }

            }
        });

        rootview.findViewById(R.id.rate_on_playstore).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.pranitkulkarni.msinusexpensecalculator"));
                startActivity(browserIntent);

            }
        });


        return rootview;
    }


    private class GetCurrencyCodes extends AsyncTask<Void,Void,Boolean>{

        List<String> codes = new ArrayList<>();

        @Override
        protected Boolean doInBackground(Void... params) {

            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL("http://api.fixer.io/latest");
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    String server_response = readStream(urlConnection.getInputStream());
                    Log.v("CatalogClient", server_response);

                    JSONObject responseObj = new JSONObject(server_response);

                    JSONObject ratesObj = responseObj.getJSONObject("rates");

                    if (ratesObj!=null){

                        for (int i = 0; i < ratesObj.length(); i++){




                        }


                    }

                    return true;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }


        @Override
        protected void onPostExecute(Boolean isResultObtained) {
            super.onPostExecute(isResultObtained);

            if (isResultObtained){




            }
        }

        // convert response to a String
        private String readStream(InputStream in) {
            BufferedReader reader = null;
            StringBuffer response = new StringBuffer();
            try {
                reader = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return response.toString();
        }
    }

}
