package com.pranitkulkarni.msinusexpensecalculator.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pranitkulkarni.msinusexpensecalculator.AdapterInfo;
import com.pranitkulkarni.msinusexpensecalculator.R;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;
import com.pranitkulkarni.msinusexpensecalculator.model.InformationModel;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHistory extends Fragment {


    public FragmentHistory() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_history, container, false);

        RecyclerView recyclerView = (RecyclerView)rootview.findViewById(R.id.recyclerView);
        TextView noInfoText = (TextView)rootview.findViewById(R.id.no_info_text);

        DatabaseManager databaseManager = new DatabaseManager(getActivity());

        final List<InformationModel> infoList = databaseManager.getAllInformation();

        if (infoList.size() > 0){

            noInfoText.setVisibility(View.GONE);

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(new AdapterInfo(getActivity(),infoList));
            recyclerView.setVisibility(View.VISIBLE);

        }
        else {

            recyclerView.setVisibility(View.GONE);
            noInfoText.setVisibility(View.VISIBLE);

        }


        return rootview;
    }

}
