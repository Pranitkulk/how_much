package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseInfo;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UniversityInformation extends AppCompatActivity {

    int info_id;
    EditText universityEt, courseEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_university_information);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        info_id = getIntent().getIntExtra("id",0);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Step 5/5");

        universityEt = (EditText)findViewById(R.id.universityName);
        courseEt = (EditText)findViewById(R.id.courseName);

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!universityEt.getText().toString().isEmpty() && !courseEt.getText().toString().isEmpty()){

                    String univName = universityEt.getText().toString();
                    String courseName = courseEt.getText().toString();


                    DatabaseManager databaseManager = new DatabaseManager(UniversityInformation.this);

                    if(databaseManager.addInfo(info_id, DatabaseInfo.Information.UNIVERSITY_NAME,univName) &&
                        databaseManager.addInfo(info_id, DatabaseInfo.Information.COURSE_NAME,courseName)){

                        Log.d("University info"," saved ");

                        Intent intent = new Intent(UniversityInformation.this,ExpensesReceipt.class);
                        intent.putExtra("id",info_id);
                        intent.putExtra("showDone",true);   // To show DONE icon
                        startActivity(intent);

                        // Show receipt...
                        //new MyCalculator(databaseManager.getInfo(info_id)).calculate();

                    }
                    else
                        Snackbar.make(coordinatorLayout,"Something went wrong! Please try again",Snackbar.LENGTH_LONG).show();

                }
                else
                    Snackbar.make(coordinatorLayout,"Enter university and course name",Snackbar.LENGTH_LONG).show();

            }
        });
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();


        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
