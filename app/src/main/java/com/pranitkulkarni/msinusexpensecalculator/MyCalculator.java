package com.pranitkulkarni.msinusexpensecalculator;

import android.util.Log;

import com.pranitkulkarni.msinusexpensecalculator.model.CalculationModel;
import com.pranitkulkarni.msinusexpensecalculator.model.InformationModel;

/**
 * Created by pranitkulkarni on 4/13/17.
 */

public class MyCalculator {

    private InformationModel model;

    public MyCalculator(InformationModel model){
        this.model = model;
    }

    public CalculationModel calculate(){

        CalculationModel calculationModel = new CalculationModel();

        Log.d("Calculation for ",model.getUniversityName()+", "+model.getCourseName());

        int per_month_living = model.getLiving_expenses()/model.getCalculated_for();
        Log.d("Per month living :",""+per_month_living);

        calculationModel.setMonthly_expense(per_month_living);

        int total_living_expenses = per_month_living * model.getDurationInMonths();


        int total_tution_cost = model.getI20Amount() - model.getLiving_expenses() - model.getStudent_funds();

        Log.d("Tution 1 :",""+total_tution_cost);

        int duration = model.getDurationInMonths();

        if (duration > 12 && duration <= 18)
            total_tution_cost = (int)(total_tution_cost * (1.5));
        else if (duration > 18 && duration <= 24)
            total_tution_cost = total_tution_cost * 2;


        Log.d("Total living ",""+total_living_expenses);
        Log.d("Total education ",""+total_tution_cost);

        calculationModel.setTotal_education_expense(total_tution_cost);
        calculationModel.setTotal_living_expense(total_living_expenses);
        calculationModel.setTotal_expense(total_living_expenses + total_tution_cost);

        return calculationModel;

    }


    public CalculationModel updateCalculation(int monthly_expense){

        CalculationModel calculationModel = new CalculationModel();
        calculationModel.setMonthly_expense(monthly_expense);

        int duration = model.getDurationInMonths();

        int total_living_expenses = monthly_expense * duration;
        calculationModel.setTotal_living_expense(total_living_expenses);

        int total_tution_cost = model.getI20Amount() - model.getLiving_expenses() - model.getStudent_funds();

        if (duration > 12 && duration <= 18)
            total_tution_cost = (int)(total_tution_cost * (1.5));
        else if (duration > 18 && duration <= 24)
            total_tution_cost = total_tution_cost * 2;

        calculationModel.setTotal_education_expense(total_tution_cost);
        calculationModel.setTotal_expense(total_living_expenses + total_tution_cost);

        return calculationModel;

    }

}
