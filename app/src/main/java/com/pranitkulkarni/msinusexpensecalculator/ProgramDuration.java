package com.pranitkulkarni.msinusexpensecalculator;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProgramDuration extends AppCompatActivity {

    int info_id,startDay,startMonth,startYear,endDay,endMonth,endYear;  // To store selection of dates
    Date startDate, endDate;    //For calculation
    String startDateInText, endDateInText;  // to save in database
    TextView startDateText, endDateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_duration);


        info_id = getIntent().getIntExtra("id",0);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Step 3/5");

        endDateText = (TextView)findViewById(R.id.end_date);
        startDateText = (TextView)findViewById(R.id.start_date);

        Calendar calendar = Calendar.getInstance();

        startDay = endDay = calendar.get(Calendar.DAY_OF_MONTH);
        startMonth = endMonth = calendar.get(Calendar.MONTH);
        startYear = endYear = calendar.get(Calendar.YEAR);

        findViewById(R.id.card_end_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(ProgramDuration.this,endDateSetListener,endYear,endMonth,endDay);
                datePickerDialog.show();

            }
        });


        findViewById(R.id.card_start_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(ProgramDuration.this,startDateSetListener,startYear,startMonth,startDay);
                datePickerDialog.show();

            }
        });


        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (startDate != null && endDate != null)
                {

                    if (endDate.after(startDate)){

                        Log.d("Start date: "+startDateInText," End date: "+endDateInText);
                        //
                        if(new DatabaseManager(ProgramDuration.this).addDates(info_id,startDateInText,endDateInText,calculateDifference())){

                            Intent intent = new Intent(ProgramDuration.this,ScholarshipInfo.class);
                            intent.putExtra("id",info_id);
                            startActivity(intent);
                        }
                        else
                            Snackbar.make(coordinatorLayout,"Something went wrong! Please try again",Snackbar.LENGTH_LONG).show();


                    }
                    else
                        Snackbar.make(coordinatorLayout,"End date cannot be before start date",Snackbar.LENGTH_LONG).show();

                }
                else
                    Snackbar.make(coordinatorLayout,"Please select dates to proceed",Snackbar.LENGTH_LONG).show();



            }
        });

    }





    DatePickerDialog.OnDateSetListener startDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            // Only for restoring selection
            startMonth = month;
            startYear = year;
            startDay = dayOfMonth;
            //


            SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
            SimpleDateFormat systemMonthFormat = new SimpleDateFormat("MM");

            SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-M-yyyy");


            try{

                month++;

                String month_name;

                month_name = monthFormat.format(systemMonthFormat.parse(String.valueOf(month)));
                startDateInText = dayOfMonth + " " + month_name + " " + year;

                startDateText.setText(startDateInText);
                startDate = myDateFormat.parse(dayOfMonth + "-" + month + "-" + year);


            }catch (Exception e){
                e.printStackTrace();
            }

        }
    };


    DatePickerDialog.OnDateSetListener endDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            // Only for restoring selection
            endMonth = month;
            endYear = year;
            endDay = dayOfMonth;
            //

            SimpleDateFormat monthFormat = new SimpleDateFormat("MMM");
            SimpleDateFormat systemMonthFormat = new SimpleDateFormat("MM");

            SimpleDateFormat myDateFormat = new SimpleDateFormat("dd-MM-yyyy");


            try{

                month++;

                String month_name;

                month_name = monthFormat.format(systemMonthFormat.parse(String.valueOf(month)));
                endDateInText = dayOfMonth + " " + month_name + " " + year;

                endDateText.setText(endDateInText);
                endDate = myDateFormat.parse(dayOfMonth + "-" + month + "-" + year);


            }catch (Exception e){
                e.printStackTrace();
            }

        }
    };


    private int calculateDifference(){

        long differenceInSeconds = (endDate.getTime() - startDate.getTime())/1000;

        long differenceInDays = differenceInSeconds / 60 / 60 / 24;

        return (int) differenceInDays/30;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
