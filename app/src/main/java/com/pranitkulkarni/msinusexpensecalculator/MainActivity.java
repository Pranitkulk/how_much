package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.pranitkulkarni.msinusexpensecalculator.fragments.FragmentHistory;
import com.pranitkulkarni.msinusexpensecalculator.fragments.FragmentHome;
import com.pranitkulkarni.msinusexpensecalculator.fragments.SettingsFragment;

import io.fabric.sdk.android.Fabric;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    private Tracker analyticsTracker;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Fragment home = new FragmentHome();
                    getSupportFragmentManager().beginTransaction().replace(R.id.content,home).commit();

                    analyticsTracker.setScreenName("Home");
                    analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());
                    return true;

                case R.id.navigation_dashboard:

                    Fragment history = new FragmentHistory();
                    getSupportFragmentManager().beginTransaction().replace(R.id.content,history).commit();

                    analyticsTracker.setScreenName("History");
                    analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());

                    return true;

                case R.id.navigation_notifications:
                    Fragment settingsFragment = new SettingsFragment();
                    getSupportFragmentManager().beginTransaction().replace(R.id.content,settingsFragment).commit();

                    analyticsTracker.setScreenName("Settings");
                    analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle(getString(R.string.app_name));

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        analyticsTracker = application.getDefaultTracker();

        Fragment home = new FragmentHome();
        getSupportFragmentManager().beginTransaction().replace(R.id.content,home).commit();


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }

}
