package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class i20Amount extends AppCompatActivity {

    EditText amountEt;
    TextView noteText;
    int calculated_for = 9;
    Tracker analyticsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_i20_amount_trial);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        analyticsTracker = application.getDefaultTracker();
        analyticsTracker.setScreenName("i20 Amount");
        analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Step 1/5");

        amountEt = (EditText)findViewById(R.id.amount);
        noteText = (TextView)findViewById(R.id.calculated_for_note);

        amountEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (( event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) || actionId == EditorInfo.IME_ACTION_DONE)
                    next();

                return false;
            }
        });



        setNoteText(getString(R.string.i20_note)+" 9 months.");


        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                next();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("onActivityResult","called");

        if (requestCode == 1){

            if (resultCode == RESULT_OK) {
                calculated_for = data.getIntExtra("months", 9);
                setNoteText(getString(R.string.i20_note) + " " +calculated_for+ " months.");
            }

        }

    }


    // Set NOTE text with updated number of months...
    private void setNoteText(String text){

        SpannableString note = new SpannableString(text);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {

                Intent changeMonths = new Intent(i20Amount.this,ChangeEstimatedMonths.class);
                changeMonths.putExtra("months",calculated_for);
                startActivityForResult(changeMonths,1);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };

        note.setSpan(clickableSpan,76,84, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        noteText.setText(note);
        noteText.setMovementMethod(LinkMovementMethod.getInstance());
        noteText.setHighlightColor(ContextCompat.getColor(i20Amount.this,R.color.colorAccent));

    }


    private void next(){

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        if (amountEt.getText().toString().length() < 3)
            Snackbar.make(coordinatorLayout,"Invalid amount",Snackbar.LENGTH_LONG).show();
        else{

            int amount = Integer.parseInt(amountEt.getText().toString());

            int row_id = new DatabaseManager(i20Amount.this).createNewEntry(amount,calculated_for); // Create new entry in the table

            if (row_id > 0) {
                Intent intent = new Intent(i20Amount.this, LivingExpenses.class);
                intent.putExtra("id",row_id);
                startActivity(intent);
            }
            else
                Snackbar.make(coordinatorLayout,"Something went wrong! Please try again",Snackbar.LENGTH_LONG).show();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
                finish();


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
