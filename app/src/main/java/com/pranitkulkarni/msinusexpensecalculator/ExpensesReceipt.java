package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;
import com.pranitkulkarni.msinusexpensecalculator.model.CalculationModel;
import com.pranitkulkarni.msinusexpensecalculator.model.InformationModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ExpensesReceipt extends AppCompatActivity {

    TextView universityName,courseName,startDate,endDate,totalExpense,totalExpenseInHomeCurrency,educationExpense,educationExpenseInHomeCurrency
            ,livingExpense,livingExpenseInHomeCurrency, monthlyExpense;
    Float conversionRate;
    InformationModel information;
    MyCalculator myCalculator;
    CalculationModel calculation,updatedCalculation;

    Tracker analyticsTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses_receipt);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        analyticsTracker = application.getDefaultTracker();
        analyticsTracker.setScreenName("Expenses receipt");
        analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        information = new DatabaseManager(ExpensesReceipt.this).getInfo(getIntent().getIntExtra("id",0));

        universityName = (TextView)findViewById(R.id.universityName);
        courseName = (TextView)findViewById(R.id.courseName);
        startDate = (TextView)findViewById(R.id.start_date);
        endDate = (TextView)findViewById(R.id.end_date);
        totalExpense = (TextView)findViewById(R.id.total_expense);
        monthlyExpense = (TextView)findViewById(R.id.monthly_expense_amount);
        totalExpenseInHomeCurrency = (TextView)findViewById(R.id.total_expense_home_currency);

        educationExpense = (TextView)findViewById(R.id.total_education_expense);
        educationExpenseInHomeCurrency = (TextView)findViewById(R.id.total_education_expense_home_currency);

        livingExpense = (TextView)findViewById(R.id.total_living_expense);
        livingExpenseInHomeCurrency = (TextView)findViewById(R.id.total_living_expense_home_currency);

        startDate.setText(information.getStartDate());
        endDate.setText(information.getEndDate());
        universityName.setText(information.getUniversityName());
        courseName.setText(information.getCourseName());

        myCalculator = new MyCalculator(information);
        calculation = myCalculator.calculate();

        updateUI(calculation);

        SeekBar seekBar = (SeekBar)findViewById(R.id.seekbar_monthly_expense);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                Log.d("Progress",""+progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                Double multiplier = (0.2)*(seekBar.getProgress() + 1);

                int predicted_amount = (int)(multiplier * calculation.getMonthly_expense());

                monthlyExpense.setText("$ "+predicted_amount);

                updatedCalculation = myCalculator.updateCalculation(predicted_amount);
                updateUI(updatedCalculation);

            }
        });

    }


    private void updateUI(CalculationModel calculation){

        totalExpense.setText("$ "+calculation.getTotal_expense());
        educationExpense.setText("$ "+calculation.getTotal_education_expense());
        livingExpense.setText("$ "+calculation.getTotal_living_expense());
        monthlyExpense.setText("$ "+calculation.getMonthly_expense());

        if (new CheckNetworkConnection(ExpensesReceipt.this).haveNetworkConnection())   // if internet is working...
            new GetResponse(calculation).execute("http://api.fixer.io/latest?base=USD");

    }


    // Get conversion rates and show amount in INR..
    private class GetResponse extends AsyncTask<String , Void ,String> {
        String server_response;
        CalculationModel calculationModel;

        public GetResponse(CalculationModel calculationModel){
            this.calculationModel = calculationModel;
        }

        @Override
        protected String doInBackground(String... strings) {

            URL url;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(strings[0]);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    server_response = readStream(urlConnection.getInputStream());
                    Log.v("CatalogClient", server_response);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            Log.e("Response", "" + server_response);

            try
            {
                JSONObject responseObj = new JSONObject(server_response);

                JSONObject rates = responseObj.getJSONObject("rates");

                // Get according to local currency or set by User

                conversionRate = Float.parseFloat(rates.getString("INR"));


                totalExpenseInHomeCurrency.setText(getString(R.string.inr_symbol) + " " + (int)(calculationModel.getTotal_expense()*conversionRate));
                educationExpenseInHomeCurrency.setText(getString(R.string.inr_symbol) + " " + (int)(calculationModel.getTotal_education_expense()*conversionRate));
                livingExpenseInHomeCurrency.setText(getString(R.string.inr_symbol) + " " + (int)(calculationModel.getTotal_living_expense()*conversionRate));

                /*Double rate_for_inr = rates.getDouble("INR");

                int value = 52000;

                int valueInRupees = (int)(value * rate_for_inr);

                Log.d("Total expense"," $52,000");
                Log.d("In Indian currency",""+valueInRupees);*/
            }
            catch (JSONException j){
                j.printStackTrace();
            }




        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (getIntent().getBooleanExtra("showDone",false))
            getMenuInflater().inflate(R.menu.menu_expenses_receipt,menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();

        if (item.getItemId() == R.id.done){

            Intent goToHome = new Intent(ExpensesReceipt.this,MainActivity.class);
            goToHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(goToHome);

        }


        return super.onOptionsItemSelected(item);
    }

    // convert response to a String
    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }


    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
