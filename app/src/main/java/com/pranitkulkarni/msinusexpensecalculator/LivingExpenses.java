package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseInfo;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LivingExpenses extends AppCompatActivity {

    EditText amountEt;
    int info_id = 0;
    Tracker analyticsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_living_expenses);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        analyticsTracker = application.getDefaultTracker();
        analyticsTracker.setScreenName("Living Expenses");
        analyticsTracker.send(new HitBuilders.ScreenViewBuilder().build());

        info_id = getIntent().getIntExtra("id",0);

        Log.d("Info id received ",""+info_id);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Step 2/5");

        amountEt = (EditText)findViewById(R.id.amount);

        amountEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (( event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) || actionId == EditorInfo.IME_ACTION_DONE)
                    next();


                return false;
            }
        });



        findViewById(R.id.nextBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                next();
            }
        });

    }

    private void next(){

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        if (amountEt.getText().toString().length() < 3)
            Snackbar.make(coordinatorLayout,"Invalid amount",Snackbar.LENGTH_LONG).show();
        else {

            final int amount = Integer.parseInt(amountEt.getText().toString());

            if (new DatabaseManager(LivingExpenses.this).addInfo(info_id, DatabaseInfo.Information.LIVING_EXPENSES,amount)){

                Intent intent = new Intent(LivingExpenses.this,ProgramDuration.class);
                intent.putExtra("id",info_id);
                startActivity(intent);

            }
            else
                Snackbar.make(coordinatorLayout,"Something went wrong! Try again",Snackbar.LENGTH_LONG).show();
        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
