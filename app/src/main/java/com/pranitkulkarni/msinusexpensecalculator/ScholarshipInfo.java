package com.pranitkulkarni.msinusexpensecalculator;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseInfo;
import com.pranitkulkarni.msinusexpensecalculator.database.DatabaseManager;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ScholarshipInfo extends AppCompatActivity {

    RadioGroup radioGroup;
    View nextLayout,amountLayout;
    EditText amountEt;
    int info_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scholarship_info);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder().build());

        info_id = getIntent().getIntExtra("id",0);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Step 4/5");

        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        nextLayout = findViewById(R.id.next_layout);
        amountLayout = findViewById(R.id.amount_layout);
        amountEt = (EditText)findViewById(R.id.amount);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                if (checkedId == R.id.radio_yes){

                    amountLayout.setVisibility(View.VISIBLE);
                    nextLayout.setVisibility(View.GONE);


                }
                else {

                    amountLayout.setVisibility(View.GONE);
                    nextLayout.setVisibility(View.VISIBLE);

                }

            }
        });


        nextLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ScholarshipInfo.this,UniversityInformation.class);
                intent.putExtra("id",info_id);
                startActivity(intent);

            }
        });

        final CoordinatorLayout coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        findViewById(R.id.nextBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!amountEt.getText().toString().isEmpty()){

                        if (new DatabaseManager(ScholarshipInfo.this).addInfo(info_id, DatabaseInfo.Information.STUDENT_FUNDS,amountEt.getText().toString())){

                            Intent intent = new Intent(ScholarshipInfo.this,UniversityInformation.class);
                            intent.putExtra("id",info_id);
                            startActivity(intent);
                        }
                        else
                            Snackbar.make(coordinatorLayout,"Something went wrong! Please try again",Snackbar.LENGTH_LONG).show();

                }
                else
                    Snackbar.make(coordinatorLayout,"Please enter your scholarship amount to proceed",Snackbar.LENGTH_LONG).show();

            }
        });




    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home)
            finish();


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context tp) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(tp));
    }
}
